package cat.itb.CRUD;

import Model.EquiposEntity;
import Model.EstadisticasEntity;
import Model.JugadoresEntity;
import cat.itb.Main;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

public class NBA_CRUD
{
    public static int getLastIDEstadisticas()
    {
        return ((EstadisticasEntity) Main.getSession()
                .createQuery("from EstadisticasEntity ORDER BY id DESC")
                .setMaxResults(1)
                .uniqueResult()).getId();
    }

    public static Boolean playerExist(final int playerID)
    {
        Query query = Main.getSession().createQuery("select 1 from JugadoresEntity t where t.id = :key");
        query.setParameter("key", playerID);

        return (query.uniqueResult() != null);
    }

    public static void insertStatistics(final EstadisticasEntity estadisticasEntity)
    {
        Session session = Main.getSession();

        if (!session.contains(estadisticasEntity))
        {
            if (session.contains(estadisticasEntity.getJugadoresByJugador()))
            {
                Transaction tx = session.beginTransaction();

                session.save(estadisticasEntity);
                try
                {
                    tx.commit();
                    System.out.println("Statistics Created Successfully!");
                }
                catch (ConstraintViolationException e)
                {
                    System.out.printf("MESSAGE: %s%n", e.getMessage());
                    System.out.printf("COD ERROR: %d%n", e.getErrorCode());
                    System.out.printf("ERROR SQL: %s%n", e.getSQLException().getMessage());
                }
            } else
                System.out.println("The player of the Statistics doesn't exist!!!");
        }
        session.close();
    }

    public static void updateWeightPlayer(final int playerID, final int weight)
    {
        Session currentSession = Main.getSession();
        if (playerExist(playerID))
        {
            Transaction tx = currentSession.beginTransaction();

            JugadoresEntity player = currentSession.load(JugadoresEntity.class, playerID);

            player.setPeso(weight);

            currentSession.update(player);

            tx.commit();

            System.out.println("Weight Updated Successfully!");
        } else System.out.println("Player doesn't exist!!!");
        currentSession.close();
    }

    public static void showStatistics(final int playerID)
    {
        Session currentSession = Main.getSession();
        if (playerExist(playerID))
        {
            JugadoresEntity player = currentSession.load(JugadoresEntity.class, playerID);

            System.out.printf("DATA OF PLAYER: %d %n" +
                    "Name: %s %n" +
                    "Team: %s %n", playerID, player.getNombre(), player.getEquiposByNombreEquipo().getNombre());

            for (final Object s : Main.getSession().createQuery("from EstadisticasEntity where jugadoresByJugador.id = :id")
                    .setParameter("id", playerID).getResultList())
            {
                System.out.println(s);
            }

        } else System.out.println("Player doesn't exist!!!");
        currentSession.close();
    }

    public static void showPlayersAverageScores()
    {
        Session currentSession = Main.getSession();

        Query getPlayerStat = currentSession.createQuery("from EstadisticasEntity where jugadoresByJugador.id = :playerID");

        var players = currentSession.createQuery("from JugadoresEntity").list();

        System.out.println("Number of teams: " + currentSession.createQuery("select count(*) from EquiposEntity").uniqueResult());
        EquiposEntity currentTeam = new EquiposEntity();
        for (final Object o : players)
        {
            JugadoresEntity player = (JugadoresEntity) o;
            EquiposEntity playerTeam = player.getEquiposByNombreEquipo();

            if (playerTeam != currentTeam)
            {
                System.out.println("\nTEAM: " + player.getNombre());
                currentTeam = player.getEquiposByNombreEquipo();
            }

            getPlayerStat.setParameter("playerID", player.getCodigo());

            float pointsAverage = 0;
            int statsCount = 1;
            for (final Object s : getPlayerStat.setParameter("playerID", player.getCodigo()).list())
            {
                pointsAverage += ((EstadisticasEntity) s).getPuntosPorPartido();
                statsCount++;
            }
            System.out.printf(" %5d, %s: %.2f %n",
                    player.getCodigo(),
                    player.getNombre(),
                    pointsAverage / statsCount);
        }
    }

    public static void showBestPlayerPerSeason()
    {
        Session currentSession = Main.getSession();

        System.out.println("Best Players Per Season Points");
        Query query = currentSession.createQuery("select 1 from EstadisticasEntity where puntosPorPartido = :maxPoints group by temporada");

        for (final Object score : currentSession.createQuery
                ("select max(puntosPorPartido) from EstadisticasEntity group by temporada").list())
        {
            EstadisticasEntity stats = (EstadisticasEntity) query.setParameter("maxPoints", score).uniqueResult();
            System.out.println("Season " + stats.getTemporada() + "\n=============================================");
            System.out.println(stats.getJugadoresByJugador()+ "\n=============================================");
        }
    }
}
