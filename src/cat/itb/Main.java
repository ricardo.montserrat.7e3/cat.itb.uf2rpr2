package cat.itb;

import Model.EstadisticasEntity;
import Model.JugadoresEntity;
import Utilities.Tools;
import cat.itb.CRUD.NBA_CRUD;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main implements Tools.Menu
{
    private static final SessionFactory ourSessionFactory;

    static
    {
        try
        {
            Configuration configuration = new Configuration();
            configuration.configure();

            ourSessionFactory = configuration.buildSessionFactory();
        }
        catch (Throwable ex)
        {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException
    {
        return ourSessionFactory.openSession();
    }

    public static void main(final String[] args)
    {
        Main program = new Main();
        program.menu("Please, select an option by number: ");
    }


    @Override
    public boolean getInputMenu(String input)
    {
        switch (input)
        {
            case "1":
                JugadoresEntity player = new JugadoresEntity(123);

                NBA_CRUD.insertStatistics(new EstadisticasEntity(
                        NBA_CRUD.getLastIDEstadisticas() + 1,
                        "05/06",
                        7.0,
                        0.0,
                        0.0,
                        5.0,
                        player
                ));

                NBA_CRUD.insertStatistics(new EstadisticasEntity(
                        NBA_CRUD.getLastIDEstadisticas() + 1,
                        "06/07",
                        10.0,
                        0.0,
                        0.0,
                        3.0,
                        player
                ));
                return false;
            case "2":
                NBA_CRUD.updateWeightPlayer(101, 240);
                NBA_CRUD.updateWeightPlayer(251, 200);
                NBA_CRUD.updateWeightPlayer(353, 300);
                NBA_CRUD.updateWeightPlayer(561, 290);
                NBA_CRUD.updateWeightPlayer(407, 263);
                return false;
            case "3":
                NBA_CRUD.showStatistics(227);
                NBA_CRUD.showStatistics(43);
                NBA_CRUD.showStatistics(469);
                return false;
            case "4": NBA_CRUD.showPlayersAverageScores(); return false;
            case "5": NBA_CRUD.showBestPlayerPerSeason(); return false;
            case "6": return true;
            default: break;
        }
        Tools.pause();
        return false;
    }

    @Override
    public void showMenu()
    {
        System.out.println("---------- Welcome to my program ----------\n\n" +
                "1.- Insert EstadisticasEntity.\n" +
                "2.- Update Peso from Jugadores.\n" +
                "3.- Show EstadisticasEntity.\n" +
                "4.- Show Average Jugadores PuntosPorPartido.\n" +
                "5.- Show best Jugador per temporada by PuntosPorPartido.\n" +
                "6.- Exit.\n");
    }
}