package Utilities;

import java.util.Scanner;

public class Tools
{
    public static void pause()
    {
        System.out.println("\n\nPress enter to continue...");
        new Scanner(System.in).nextLine();
    }

    public static double inputDouble(String message, boolean canBeNegative)
    {
        Scanner reader = new Scanner(System.in);
        boolean isNumber;
        double userNum = 0;
        do
        {
            try
            {
                System.out.print(message);
                userNum = reader.nextDouble();
                isNumber = true;
            }
            catch (Exception e) { isNumber = false; }
            if (!isNumber) reader.nextLine();
        }
        while (!isNumber || userNum < 0 && !canBeNegative);
        return userNum;
    }

    public static int inputInt(String message, boolean canBeNegative)
    {
        Scanner reader = new Scanner(System.in);
        boolean isNumber;
        int userNum = 0;
        do
        {
            try
            {
                System.out.print(message);
                userNum = reader.nextInt();
                isNumber = true;
            }
            catch (Exception e) { isNumber = false; }
            if (!isNumber) reader.nextLine();
        }
        while (!isNumber || userNum < 0 && !canBeNegative);
        return userNum;
    }


    public static String inputAny(String message)
    {
        System.out.print(message);
        return new Scanner(System.in).nextLine();
    }

    public interface Menu
    {
        default void menu(String menuSelectMessage)
        {
            boolean finish;
            do
            {
                showMenu();
                finish = getInputMenu(Tools.inputAny(menuSelectMessage));
            }
            while (!finish);
            System.out.println("Goodbye!! :D");
        }

        boolean getInputMenu(String input);
        void showMenu();
    }

}
